Microsoft visual studio versão 2015
Banco de dados SQLServer versão 2019

Foi utilizado um banco de dados com nome de CINEMA,e está configurado para autenticação no usuário do windows.
Caso precise trocar a autenticação, basta ir nas propriedades > Settings e alterar a string de conexão.
Existe 2 string de conexão, DB_MASTER é para conectar diretamente na master do SQLServer e rodar o comando para criar a base CINEMA
Conexão CINEMA serve para fazer todos os processos de inclusão de tabelas e registros.

É possivel instanciar a base e suas tabelas diretamente pelo sistema, basta acessar http://seu.diretio.aqui/Migrations ex (http://localhost:50770/Migrations), preencher com a senha : admin e executar os processos para criar a base e então suas tabelas respectivamente.
Caso seja necessário, irei disponibilizar um arquivo na raiz do projeto contendo todos os scripts necessários para executar o projeto sem a necessidade de utilizar a rota de migrations. Nome do arquivo : scripts.txt
Para fazer o login no sistema será necessário ter as 5 tabelas cadastradas ('usuarios', 'filmes', 'salas', 'sessoes', 'configs')
Após configurado o ambiente e o banco de dados é hora de acessar o sistema, na home basta clicar em 'login' para fazer a autenticação. Essa, por sua vez poderá ser feita de forma mista, apenas com o nome do usuário, nome e sobrenome ou email.
Dados de admin para poder realizar a autenticação :
usuario : 'admin'
sobrenome : ' do sistema'
email : 'admin@admin.com'
senha : 123
ou u
suario : 'gerente'
sobrenome : ' do sistema'
email : 'gerente@gerente.com'
senha : '123'

sedo assim são algumas das opções para fazer o login
usuario : 'admin'
senha : '123'
usuario : 'admin do sistema'
senha : '123'
usuario : 'admin@admin.com'
senha : '123'

usuario : 'gerente'
senha : '123'
usuario : 'gerente do sistema'
senha : '123'
usuario : 'gerente@gerente.com'
senha : '123'


Feito isso o proximo passo é cadastrar o filme, no menu superior clique em 'filmes'.
Nessa tela é possivel fazer buscas por titulos e tambem verificar quais os filmes estão em cartaz ou fora de cartaz.
Para incluir um novo filme clique em 'Novo'.
Basta preencher o titulo, descrição e a duração em minutos, a imagem é opcional, assim como o campo 'em cartaz' que por padrão está desmarcado, mas lembre-se que para poder vincular o filme a sessão futuramente precisamos de um filme em cartaz.
Após preencher e clicar em salvar, será redirecionado para a tela do index, onde tem a listagem dos filmes.

Para incluir uma sessão basta cliclar em 'Sessões' no menu superior.
Agora basta clicar em 'Novo' para pode incluir a nova sessão.
Será necessário preencher uma data/hora válida e buscar por um filme, basta digitar pelo menos 1 letra pra fazer a pesquisa.
Lembre-se de também selecionar a sala, tipo de animação (2D ou 3D) , tipo de audio (Legendado ou Dublado) e por final o valor do ingresso.
Ainda na tela de inclução existe alguns campos que não podem ser alterados como Duração do filme(Minutos), que é o valor cadastrado diretamente no filme, o Tempo de limpeza da sala que está em Configurações, e a Data/Hora Final da sessão, esse é a soma da duração do filme + o tempo de limpeza da sala, levando em conta a data/hora informada na parte superior do cadastro.
Preenchendo todos os campos basta clicar em 'Salvar'.

Pronto, agora temos uma sessão feita. lembrando que ela só poderá ser excluida se o inicio dela for maior que o prazo para exclusão de sessões em configurações.

Outros registros:
Salas são os registros pré definidos do sistema, voce não poderá alterar, excluir ou incluir uma nova, mas poderá visualizar as já cadastradas.
Usuário, assim coma a sala são apenas registro de visualizações.
Configurações esse é o registro onde você poderá definir o tempo de limpeza da sala, que irá somar no tempo total de uma sessão, e o tempo que o usuário poderá excluir uma sessão.
Os valores padrões são 10 minutos para limpeza de sala, e 14400 minutos (10 dias) para excluir um filme.
Para alterar basta preencher o novo valor desejado e clicar em 'Salvar Configurações'

