﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using GerenciamentoCinema.Controllers;
using GerenciamentoCinema.Models;

namespace GerenciamentoCinema.Tests.Controllers {
	[TestClass]
	public class SessaoControllerTest {
		public void setUp() {
			GeneralController.isTeste = true;
		}

		/* view/get metodos */
		[TestMethod]
		public void Index() {
			setUp();
			SessaoController controller = new SessaoController();
			ViewResult result = controller.Index() as ViewResult;
			Assert.AreEqual("Index", result.ViewName);
		}


		[TestMethod]
		public void Novo() {
			setUp();
			SessaoController controller = new SessaoController();
			ViewResult result = controller.Novo() as ViewResult;
			Assert.AreEqual("Novo", result.ViewName);
		}

		/* http to ajax*/
		[TestMethod]
		public void IndexPG() {
			setUp();
			SessaoController controller = new SessaoController();
			JsonResult result = controller.IndexPG(1, "") as JsonResult;
			Assert.IsNotNull(result);
		}


		[TestMethod]
		public void Visualizar() {
			setUp();
			SessaoController controller = new SessaoController();
			JsonResult result = controller.Visualizar(0) as JsonResult;
			Assert.IsNotNull(result);
		}

		/* rotinas */
		[TestMethod]
		public void forceHardDelete() {
			FilmeControllerTest filmeTeste = new FilmeControllerTest();
			//deleta hard filme
			Assert.AreEqual(true, filmeTeste.deletaHard());
			// deleta hard sessao
			Assert.AreEqual(true, deletaHard());
		}

		[TestMethod]
		public void NovoDeleta() {
			setUp();
			FilmeControllerTest filmeTeste = new FilmeControllerTest();
			int id_filme = filmeTeste.NovoModel();
			int id_sala = 1;
			int id_sessao = NovoModel(id_filme, id_sala);
			// tenta deletar o filme, precisa dar erro pois tem vinculo com a sessao
			Assert.AreEqual(false, filmeTeste.deleta(id_filme));
			
			// deleta soft sessao
			Assert.AreEqual(true, deleta(id_sessao));
			//deleta soft filme
			Assert.AreEqual(true, filmeTeste.deleta(id_filme));

			//deleta hard filme
			Assert.AreEqual(true, filmeTeste.deletaHard());
			// deleta hard sessao
			Assert.AreEqual(true, deletaHard());
		}

		public int NovoModel(int id_filme, int id_sala) {
			SessaoController controller = new SessaoController();
			SessaoModel sm = new SessaoModel();
			sm.id_filme = id_filme;
			sm.id_sala = id_sala;
			sm.tp_animacao = "2D";
			sm.tp_audio = "L";
			sm.valor_ingresso = 1;
			// data no futuro para nao ter problema para deletar devido a restrição da configuração
			// se colocar data dentro do range que não pode ser deletado irá causar erro no soft delete da sessao
			sm.datepicker = "13/12/2021"; // dd/mm/yyyy
			sm.timepicker = "08:00"; // HH:mm
			var result = controller.Novo(sm);

			var r = result.GetType();
			if (r.Name == "RedirectToRouteResult") { // retornou a RedirectToRouteResult então conseguiu incluir a sessao
				result = controller.Novo(sm); // tenta realizar a inclusão de uma nova sessao com as mesmas propriedades
				r = result.GetType();
				if (r.Name == "RedirectToRouteResult") {  // retornou a RedirectToRouteResult então conseguiu incluir o sessao, novamente
					Assert.Fail("Incluir um filme repetido"); // fail pois incluiu 2 sessoes no mesmo horario/dia/sala
				} else {
					// pega id da sessao
					DateTime datahora = DateTime.Parse(sm.datepicker + " " + sm.timepicker);
					sm.data_hora_formatada = datahora.ToString("yyyyMMdd HH:mm:ss");

					JsonResult resultJSONDelete = controller.locadeByFilmeSalaDataHora(id_filme,id_sala, sm.data_hora_formatada) as JsonResult;
					string resultStrJson = resultJSONDelete.Data.ToString();
					resultStrJson = resultStrJson.Replace("{", "").Replace("}", "").Replace("sessao_id = ", "");
					int id_sessao = Int32.Parse(resultStrJson);
					return id_sessao;
				}
			} else {
				Assert.Fail("Incluir um filme já cadastrado"); // fail pois incluiu um filme que já estava cadastrado
			}

			return 0;
		}

		public Boolean deleta(int id_sessao) {
			SessaoController sessaoTeste = new SessaoController();
			// soft delete
			JsonResult resultDelete = sessaoTeste.deletar(id_sessao) as JsonResult;
			return ("{ msg =  }" == resultDelete.Data.ToString());
		}

		public Boolean deletaHard() {
			SessaoController sessaoTeste = new SessaoController();
			//hard delete
			JsonResult resultHardDelete = sessaoTeste.HardDelete() as JsonResult;
			return ("{ deleted_all = True }" == resultHardDelete.Data.ToString());
		}


	}
}
