﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GerenciamentoCinema;
using GerenciamentoCinema.Controllers;
using GerenciamentoCinema.Models;
using Newtonsoft.Json.Linq;

namespace GerenciamentoCinema.Tests.Controllers {
	[TestClass]
	public class FilmeControllerTest {

		public void setUp() {
			GeneralController.isTeste = true;
		}

		/* view/get metodos */
		[TestMethod]
		public void Index() {
			setUp();
			FilmeController controller = new FilmeController();
			ViewResult result = controller.Index() as ViewResult;
			Assert.AreEqual("Index", result.ViewName);
		}


		[TestMethod]
		public void Novo() {
			setUp();
			FilmeController controller = new FilmeController();
			ViewResult result = controller.Novo() as ViewResult;
			Assert.AreEqual("Novo", result.ViewName);
		}

		/* http to ajax*/
		[TestMethod]
		public void IndexPG() {
			setUp();
			FilmeController controller = new FilmeController();
			JsonResult result = controller.IndexPG(1, "") as JsonResult;
			Assert.IsNotNull(result);
		}

		[TestMethod]
		public void ListEmCartaz() {
			setUp();
			FilmeController controller = new FilmeController();
			JsonResult result = controller.ListEmCartaz(1, "") as JsonResult;
			Assert.IsNotNull(result);
		}


		[TestMethod]
		public void Visualizar() {
			setUp();
			FilmeController controller = new FilmeController();
			JsonResult result = controller.Visualizar(0) as JsonResult;
			Assert.IsNotNull(result);
		}

		/* rotinas */
		[TestMethod]
		public void forceHardDelete() {
			FilmeControllerTest filmeTeste = new FilmeControllerTest();
			//deleta hard filme
			Assert.AreEqual(true, filmeTeste.deletaHard());
		}

		[TestMethod]
		public void NovoEDeleta() {
			setUp();
			int id_filme = NovoModel();
			if (id_filme > 0) {
				//soft delete
				Assert.AreEqual(true, deleta(id_filme));
				//hard delete
				Assert.AreEqual(true, deletaHard());
			}			
		}

		public int NovoModel() {
			FilmeController controller = new FilmeController();
			FilmeModel filme = new FilmeModel();
			/* cria uma string generica para não ter problema de tentar cadastrar um filme já existente*/
			var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			var stringChars = new char[20];
			var random = new Random();

			for (int i = 0; i < stringChars.Length; i++) {
				stringChars[i] = chars[random.Next(chars.Length)];
			}

			filme.titulo = new String(stringChars);
			filme.descricao = new String(stringChars);
			//filme.titulo = "Teste";
			//filme.descricao = "Teste";
			filme.duracao_min = 1;
			filme.em_cartaz = true;
			filme.imagem = "";

			var result = controller.Novo(filme);

			var r = result.GetType();
			if (r.Name == "RedirectToRouteResult") { // retornou a RedirectToRouteResult então conseguiu incluir o filme
				result = controller.Novo(filme); // tenta realizar a inclusão de um novo filme com as mesmas propriedades
				r = result.GetType();
				if (r.Name == "RedirectToRouteResult") {  // retornou a RedirectToRouteResult então conseguiu incluir o filme, novamente
					Assert.Fail("Incluir um filme repetido"); // fail pois incluiu 2 filmes com o mesmo nome
				} else {
					// pega id do filme
					JsonResult resultJSONDelete = controller.locateIDByTitulo(filme.titulo) as JsonResult;
					string resultStrJson = resultJSONDelete.Data.ToString();
					resultStrJson = resultStrJson.Replace("{", "").Replace("}", "").Replace("filme_id = ", "");
					int id_filme = Int32.Parse(resultStrJson);
					return id_filme;				
				}
			}else {
				Assert.Fail("Incluir um filme já cadastrado"); // fail pois incluiu um filme que já estava cadastrado
			}

			return 0;
		}

		public Boolean deleta(int id_filme) {
			FilmeController controller = new FilmeController();
			// soft delete
			JsonResult resultDelete = controller.deletar(id_filme) as JsonResult;
			return  ("{ msg =  }" == resultDelete.Data.ToString());
		}

		public Boolean deletaHard() {
			FilmeController controller = new FilmeController();
			//hard delete
			JsonResult resultHardDelete = controller.HardDelete() as JsonResult;
			return  ("{ deleted_all = True }" == resultHardDelete.Data.ToString());
		}

	}
}
