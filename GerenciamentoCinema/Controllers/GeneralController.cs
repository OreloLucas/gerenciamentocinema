﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace GerenciamentoCinema.Controllers {
	public class GeneralController : Controller {
		public const int MaxPPag = 10;
		public static Boolean isTeste { get; set; } = false;

		public ActionResult Index() {
			return View();
		}

		public Boolean RequestAuth() {
			if (Session == null) {
				Session.Add("auth_user_name", "admin");
				Session.Add("auth", true);
				return (Convert.ToBoolean(Session["auth"]));
				//return false;
			} else {
				return (Convert.ToBoolean(Session["auth"]));
			}
		}

		public static Boolean isAuth() {
			if (isTeste) {
				isTeste = false;
				return true;				
			}
			return (Convert.ToBoolean(System.Web.HttpContext.Current.Session["auth"]));
		}
		public int checkTempDataToast(TempDataDictionary tempData) {
			int i = 0;

			do {
				if (TempData["html" + i.ToString()] != null) {
					i++;
				}
			} while (TempData["html" + i.ToString()] != null);
			return i;
		}

		internal static Tuple<int, string> sendToast(int lastIndex, string type, string msg, string title = "") {
			StringBuilder s = new System.Text.StringBuilder();
			s.Append(" <script> ");
			s.Append("$(document).ready(function () {");
			s.Append("toastr.options = { ");
			s.Append("					'closeButton': false, ");
			s.Append("					'debug': false, ");
			s.Append("					'newestOnTop': false, ");
			s.Append("					'progressBar': true, ");
			s.Append("					'positionClass': 'toast-top-right', ");
			s.Append("					'preventDuplicates': false, ");
			s.Append("					'onclick': null, ");
			s.Append("					'showDuration': '300', ");
			s.Append("					'hideDuration': '1000', ");
			s.Append("					'timeOut': '3000', ");
			s.Append("					'extendedTimeOut': '1000', ");
			s.Append("					'showEasing': 'swing', ");
			s.Append("					'hideEasing': 'linear', ");
			s.Append("					'showMethod': 'fadeIn', ");
			s.Append("					'hideMethod': 'fadeOut', ");
			s.Append("					'allowHtml' : true  }; ");
			s.Append("toastr['" + type + "'](\"" + msg + "\", \"" + title + "\"); }); </script>");
			//string index = "html" + new GeneralController().checkTempDataToast(tempData).ToString();
			lastIndex++;
			return Tuple.Create(lastIndex, s.ToString());
		}
	}
}
