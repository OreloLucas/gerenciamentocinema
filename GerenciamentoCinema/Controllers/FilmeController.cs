﻿using GerenciamentoCinema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using GerenciamentoCinema.Controllers;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using System.IO;

namespace GerenciamentoCinema.Controllers {


	public class FilmeController : Controller {

		public const string tablename = "filmes";
		public ActionResult Index() {
			if (GeneralController.isAuth()) {
				ViewBag.Title = "Cadastros de filmes";
				var result = GeneralModel.getTotalPaginas(tablename, " where deleted = 0");
				ViewBag.TotPag = result.Item1;
				ViewBag.TotReg = result.Item2;
				ViewBag.RegPorPag = GeneralController.MaxPPag;
				ViewBag.PagAtual = 1;
				return View("Index");
			} else {
				return RedirectToAction("deny", "Access");
			}
		}

		[HttpPost]
		public JsonResult IndexPG(int pg, string busca, string filtro_status = "") {
			if (GeneralController.isAuth()) {
				switch (filtro_status) {
					case "emcartaz":
						filtro_status = "AND em_cartaz = 1 ";
						break; //somente filmes em cartaz
					case "foracartaz":
						filtro_status = "AND em_cartaz = 0";
						break; //somente filmes fora de cartaz
					default:
						filtro_status = " AND 1=1 ";
						break; //Sem restrição de filtro

				};

				var list = FilmeModel.listLimmit(pg, busca, filtro_status);
				ViewBag.MaxPPag = GeneralController.MaxPPag;
				ViewBag.PagAtual = pg;
				return Json(list);
			} else {
				return null;
			}
		}

		[HttpPost]
		public JsonResult ListEmCartaz(int pg, string busca) {
			if (GeneralController.isAuth()) {
				var list = FilmeModel.listLimmit(pg, busca, " AND em_cartaz = 1");
				ViewBag.MaxPPag = GeneralController.MaxPPag;
				ViewBag.PagAtual = pg;
				return Json(list);
			} else {
				return null;
			}
		}


		public ActionResult Novo() {
			if (GeneralController.isAuth()) {
				ViewBag.Title = "Novo filme";
				return View("Novo");
			} else {
				return RedirectToAction("deny", "Access");
			}
		}

		[HttpPost]
		public ActionResult Novo(FilmeModel filme) {
			ViewBag.Title = "Novo filme";
			if (!ModelState.IsValid) {
				return View(filme);
			} else {
				var result = FilmeModel.Insert(filme);
				if (result == "") {
					var resultToast = GeneralController.sendToast(TempData["html_last_index"] == null ? -1 : (int)TempData["html_last_index"], "success", "Filme incluido com sucesso");
					TempData["html_last_index"] = resultToast.Item1;
					TempData["html" + resultToast.Item1] = resultToast.Item2;
					return RedirectToAction("Index");
				} else {
					var resultToast = GeneralController.sendToast(TempData["html_last_index"] == null ? -1 : (int)TempData["html_last_index"], "warning", result);
					TempData["html_last_index"] = resultToast.Item1;
					TempData["html" + resultToast.Item1] = resultToast.Item2;
					ModelState.Clear();
					return View(filme);
				}
			}
		}

		public ActionResult Editar(int id) {
			ViewBag.Title = "Editar filme " + id.ToString();
			if (GeneralController.isAuth()) {
				FilmeModel a = FilmeModel.locateByID(id);
				if (a.id <= 0) {// não achou o filme
					var resultToast = GeneralController.sendToast(TempData["html_last_index"] == null ? -1 : (int)TempData["html_last_index"], "warning", "Filme não localizado");
					TempData["html_last_index"] = resultToast.Item1;
					TempData["html" + resultToast.Item1] = resultToast.Item2;
					return RedirectToAction("Index");
				} else {
					return View(a);
				}

			} else {
				return RedirectToAction("deny", "Access");
			}
		}

		[HttpPost]
		public JsonResult Visualizar(int id) {
			if (GeneralController.isAuth()) {
				FilmeModel a = FilmeModel.locateByID(id);
				if (a.id <= 0) {// não achou o filme
					return Json(new { msg = "Filme não localizado" });
				} else {
					return Json(a);
				}

			} else {
				return null;
			}
		}

		[HttpPost]
		public ActionResult Editar(FilmeModel filme) {
			ViewBag.Title = "Editar filme";
			if (!ModelState.IsValid) {
				return View(filme);
			} else {
				var result = FilmeModel.Update(filme);
				if (result == "") {
					var resultToast = GeneralController.sendToast(TempData["html_last_index"] == null ? -1 : (int)TempData["html_last_index"], "success", "Filme editado com sucesso");
					TempData["html_last_index"] = resultToast.Item1;
					TempData["html" + resultToast.Item1] = resultToast.Item2;

					return RedirectToAction("Index");
				} else {
					var resultToast = GeneralController.sendToast(TempData["html_last_index"] == null ? -1 : (int)TempData["html_last_index"], "warning", result);
					TempData["html_last_index"] = resultToast.Item1;
					TempData["html" + resultToast.Item1] = resultToast.Item2;

					return View(filme);
				}
			}
		}

		[HttpPost]
		public JsonResult canDelete(int id) {
			return Json(FilmeModel.canDelete(id));
		}

		[HttpPost]
		public JsonResult deletar(int id) {
			var result = FilmeModel.delete(id);
			if (result == "") {
				var resultToast = GeneralController.sendToast(TempData["html_last_index"] == null ? -1 : (int)TempData["html_last_index"], "success", "Filme deletado com sucesso");
				TempData["html_last_index"] = resultToast.Item1;
				TempData["html" + resultToast.Item1] = resultToast.Item2;
				return Json(new { msg = result });
			} else {
				var resultToast = GeneralController.sendToast(TempData["html_last_index"] == null ? -1 : (int)TempData["html_last_index"], "error", result);
				TempData["html_last_index"] = resultToast.Item1;
				TempData["html" + resultToast.Item1] = resultToast.Item2;
				return Json(new { msg = result });
			}
		}

		[HttpPost]
		public JsonResult locateIDByTitulo(string name) {
			int filme_id = FilmeModel.locateIDByTitulo(name);
			return Json(new { filme_id });
		}

		[HttpPost]
		public JsonResult HardDelete() {
			Boolean deleted_all = FilmeModel.HardDelete();
			return Json(new { deleted_all });
		}


	}
}