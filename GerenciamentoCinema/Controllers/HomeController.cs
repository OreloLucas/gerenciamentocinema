﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using GerenciamentoCinema.Models;

namespace GerenciamentoCinema.Controllers {
	public class HomeController : Controller {
		public ActionResult Index() {
			ViewBag.Title = "Home";
			if (Convert.ToBoolean(Session["auth"])) {
				return RedirectToAction("Home", "Home");
			} else {
				return View();
			}

			
		}
		public ActionResult Home() {
			if (!Convert.ToBoolean(Session["auth"])) {
				return RedirectToAction("Index", "Home");
			} else {
				return View();
			}
		}

		
		public ActionResult Login() {
			ViewBag.Title = "Login";
			if (Convert.ToBoolean(Session["auth"])) {
				return RedirectToAction("Home", "Home");
			} else {
				return View();
			}
		}

		[HttpPost]
		public ActionResult Login(UserModel login) {
			if (!ModelState.IsValid) {
				return View(login);
			}

			var username = UserModel.isValidUser(login.user, login.password); 
			
			if (username != "") {
				Session.Add("auth_user_name", username);
				Session.Add("auth", true);
				Session.Timeout = 60;
				return RedirectToAction("Index", "Home");				
			} else {
				ModelState.AddModelError("", "Login inválido.");
				Session.Add("auth", false);
			}

			return View(login);
		}

		public ActionResult LogOff() {
			Session.Add("auth", false);
			return RedirectToAction("Index", "Home");
		}
	}
}