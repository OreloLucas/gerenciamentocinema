﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GerenciamentoCinema.Models;

namespace GerenciamentoCinema.Controllers {
	public class UserController : Controller {
		public const string tablename = "usuarios";

		public ActionResult Index() {
			if (GeneralController.isAuth()) {
				ViewBag.Title = "Cadastros de usuários";
				var result = GeneralModel.getTotalPaginas(tablename);
				ViewBag.TotPag = result.Item1;
				ViewBag.TotReg = result.Item2;
				ViewBag.RegPorPag = GeneralController.MaxPPag;
				ViewBag.PagAtual = 1;
				return View();
			} else {
				return RedirectToAction("deny", "Access");
			}
		}

		[HttpPost]
		public JsonResult IndexPG(int pg) {
			if (GeneralController.isAuth()) {
				var list = UserModel.listLimmit(pg);
				ViewBag.MaxPPag = GeneralController.MaxPPag;
				ViewBag.PagAtual = pg;
				return Json(list);
			} else {
				return null;
			}
		}

	}
}