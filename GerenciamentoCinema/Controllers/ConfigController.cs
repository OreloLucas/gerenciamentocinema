﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GerenciamentoCinema.Controllers
{
    public class ConfigController : Controller
    {
        // GET: Config
        public ActionResult Index()
        {
            if (GeneralController.isAuth())
            {
                ViewBag.Title = "Configurações do sistema";
                return View();
            }
            else
            {
                return RedirectToAction("deny", "Access");
            }
        }

        [HttpPost]
        public JsonResult Save(ConfigModel config)
        {
            return Json(ConfigModel.Save(config));
        }

        [HttpPost]
        public JsonResult Load()
        {
            return Json(ConfigModel.Load());
        }



        public static int getTempoLimpeza()
        {
            return ConfigModel.getTempoLimpeza();
        }

    }
}