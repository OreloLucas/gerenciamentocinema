﻿using GerenciamentoCinema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace GerenciamentoCinema.Controllers {
	public class SessaoController : Controller {
		public const string tablename = "sessoes";
		public ActionResult Index() {
			if (GeneralController.isAuth()) {
				ViewBag.Title = "Cadastros de Sessões";
				var result = GeneralModel.getTotalPaginas(tablename, " where deleted = 0");
				ViewBag.TotPag = result.Item1;
				ViewBag.TotReg = result.Item2;
				ViewBag.RegPorPag = GeneralController.MaxPPag;
				ViewBag.PagAtual = 1;
				return View("Index");
			} else {
				return RedirectToAction("deny", "Access");
			}
		}

		[HttpPost]
		public JsonResult IndexPG(int pg, string filtro = "todas") {
			if (GeneralController.isAuth()) {
				var list = SessaoModel.listLimmit(pg, filtro);
				ViewBag.MaxPPag = GeneralController.MaxPPag;
				ViewBag.PagAtual = pg;
				return Json(list);
			} else {
				return null;
			}
		}

		public ActionResult Novo() {
			if (GeneralController.isAuth()) {
				ViewBag.Title = "Nova Sessão";
				SessaoModel s = new SessaoModel();
				s.tempo_limpeza = ConfigModel.getTempoLimpeza();
				return View("Novo", s);
			} else {
				return RedirectToAction("deny", "Access");
			}
		}

		[HttpPost]
		public ActionResult Novo(SessaoModel sessao) {
			ViewBag.Title = "Nova Sessão";
			ViewBag.tp_animacao = sessao.tp_animacao;
			ViewBag.tp_audio = sessao.tp_audio.ToUpper() == "LEGENDADO" ? "L" : "D";
			ViewBag.sala = sessao.id_sala;
			ViewBag.timepicker = sessao.timepicker;
			ViewBag.datepicker = sessao.datepicker;
			if (!ModelState.IsValid) {
				return View(sessao);
			} else {
				DateTime datahora = DateTime.Parse(sessao.datepicker + " " + sessao.timepicker);
				sessao.data_hora_formatada = datahora.ToString("yyyyMMdd HH:mm:ss");
				var result = SessaoModel.Insert(sessao);
				if (result == "") {
					var resultToast = GeneralController.sendToast(TempData["html_last_index"] == null ? -1 : (int)TempData["html_last_index"], "success", "Sessão incluido com sucesso");
					TempData["html_last_index"] = resultToast.Item1;
					TempData["html" + resultToast.Item1] = resultToast.Item2;
					return RedirectToAction("Index");
				} else {
					var resultToast = GeneralController.sendToast(TempData["html_last_index"] == null ? -1 : (int)TempData["html_last_index"], "warning", result);
					TempData["html_last_index"] = resultToast.Item1;
					TempData["html" + resultToast.Item1] = resultToast.Item2;
					ModelState.Clear();
					return View(sessao);
				}
			}
		}



		[HttpPost]
		public JsonResult checkHorarioValido(string inicio, int idsala, int id_filme, int id_sessao = 0) {
			inicio = inicio.Replace("-", "");
			return Json(SessaoModel.checkHorarioValido(inicio, idsala, id_filme, id_sessao));
		}

		public ActionResult Editar(int id) {
			ViewBag.Title = "Editar Sessão";
			SessaoModel sessao = SessaoModel.locateByID(id);
			ViewBag.tp_animacao = sessao.tp_animacao;
			ViewBag.tp_audio = sessao.tp_audio.ToUpper() == "LEGENDADO" ? "L" : "D";
			ViewBag.sala = sessao.id_sala;
			ViewBag.timepicker = sessao.timepicker;
			ViewBag.datepicker = sessao.datepicker;
			return View(sessao);
		}

		[HttpPost]
		public ActionResult Editar(SessaoModel sessao) {
			ViewBag.Title = "Editar Sessão";
			ViewBag.tp_animacao = sessao.tp_animacao;
			ViewBag.tp_audio = sessao.tp_audio.ToUpper() == "LEGENDADO" ? "L" : "D";
			ViewBag.sala = sessao.id_sala;
			ViewBag.timepicker = sessao.timepicker;
			ViewBag.datepicker = sessao.datepicker;
			if (!ModelState.IsValid) {
				return View(sessao);
			} else {
				DateTime datahora = DateTime.Parse(sessao.datepicker + " " + sessao.timepicker);
				sessao.data_hora_formatada = datahora.ToString("yyyyMMdd HH:mm:ss");
				var result = SessaoModel.Update(sessao);
				if (result == "") {
					var resultToast = GeneralController.sendToast(TempData["html_last_index"] == null ? -1 : (int)TempData["html_last_index"], "success", "Sessão editada com sucesso");
					TempData["html_last_index"] = resultToast.Item1;
					TempData["html" + resultToast.Item1] = resultToast.Item2;
					return RedirectToAction("Index");
				} else {
					var resultToast = GeneralController.sendToast(TempData["html_last_index"] == null ? -1 : (int)TempData["html_last_index"], "warning", result);
					TempData["html_last_index"] = resultToast.Item1;
					TempData["html" + resultToast.Item1] = resultToast.Item2;
					ModelState.Clear();
					return View(sessao);
				}
			}
		}


		[HttpPost]
		public JsonResult Visualizar(int id) {
			return Json(SessaoModel.locateByID(id));
		}

		[HttpPost]
		public JsonResult locadeByFilmeSalaDataHora(int id_filme, int id_sala, string datahora) {
			int sessao_id = SessaoModel.locadeIDByFilmeSalaDataHora(id_filme, id_sala, datahora);
			return Json(new { sessao_id });

		}

		[HttpPost]
		public JsonResult deletar(int id) {
			var result = SessaoModel.delete(id);
			if (result == "") {
				var resultToast = GeneralController.sendToast(TempData["html_last_index"] == null ? -1 : (int)TempData["html_last_index"], "success", "Sessão deletado com sucesso");
				TempData["html_last_index"] = resultToast.Item1;
				TempData["html" + resultToast.Item1] = resultToast.Item2;
				return Json(new { msg = result });
			} else {
				var resultToast = GeneralController.sendToast(TempData["html_last_index"] == null ? -1 : (int)TempData["html_last_index"], "error", result);
				TempData["html_last_index"] = resultToast.Item1;
				TempData["html" + resultToast.Item1] = resultToast.Item2;
				return Json(new { msg = result });
			}
		}

		[HttpPost]
		public JsonResult HardDelete() {
			Boolean deleted_all = SessaoModel.HardDelete();
			return Json(new { deleted_all });
		}

	}
}