﻿using System;
using System.Web.Mvc;
using System.Text;
using GerenciamentoCinema.Properties;
using GerenciamentoCinema.Models;

namespace GerenciamentoCinema.Controllers {
	public class MigrationsController : Controller {

		public ActionResult Index() {

			return View();
		}

		public string checkAuth(string password_migration) {
			if (password_migration == "admin") {
				return "";
			} else {
				return "senha incorreta";
			}
		}


		[HttpPost]
		public JsonResult check() {
			return Json(MigrationsModel.checkDados());
		}

		[HttpPost]
		public ActionResult createDatabase(string password_migration) {
			string chk = checkAuth(password_migration);

			if (chk == "") {
				return Json(MigrationsModel.createDatabase());
			} else {
				return Json(new { msg_erro = chk });
			}
		}

		[HttpPost]
		public ActionResult UpdateTables(string password_migration) {
			string chk = checkAuth(password_migration);

			if (chk == "") {
				return Json(MigrationsModel.UpdateTables());
			} else {
				return Json(new { msg_erro = chk });
			}
		}

		[HttpPost]
		public ActionResult dropDatabase(string password_migration) {
			string chk = checkAuth(password_migration);

			if (chk == "") {
				return Json(MigrationsModel.DropDatabase());
			} else {
				return Json(new { msg_erro = chk });
			}
		}

		[HttpPost]
		public ActionResult dropTables(string password_migration) {
			string chk = checkAuth(password_migration);

			if (chk == "") {
				return Json(MigrationsModel.DropTables());
			} else {
				return Json(new { msg_erro = chk });
			}
		}

		


	}
}