﻿using GerenciamentoCinema.Properties;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace GerenciamentoCinema.Models {
	public class MigrationsModel {

		static string msg_database = "";
		static string msg_tabelas = "";
		static string msg_erro = "";
		static bool create_database = false;
		static bool update_tables = false;
		static bool drop_database = false;
		static bool drop_table = false;
		static bool status_db_ok = false;
		static bool status_table_ok = false;

		public static Object checkDados() {
			SqlConnection conn = null;
			SqlDataReader rdr = null;
		
			try {
				conn = new SqlConnection(Settings.Default.DB_MASTER);
				conn.Open();
				SqlCommand cmd = new SqlCommand();
				cmd.CommandText = " SELECT 1 FROM sys.databases WHERE name = 'cinema' ";
				cmd.Connection = conn;
				SqlDataAdapter da = new SqlDataAdapter();
				da.SelectCommand = cmd;
				DataSet ds = new DataSet();
				da.Fill(ds);
				if (ds.Tables.Count > 0) //if database exist
				{
					msg_database = "Database Criada";
					create_database = false;
					update_tables = true;
					drop_database = true;
					drop_table = false;
					status_db_ok = true;
				} else { //if not database exist
					msg_database = "Database ainda não criada ou não localizada";
					create_database = true;
					update_tables = false;
					drop_database = false;
					drop_table = false;
				}
			} catch (Exception ex) {
				msg_erro = ex.Message;
			} finally {
				if (conn != null) {
					conn.Close();
				}
				if (rdr != null) {
					rdr.Close();
				}
			}

			try {
				conn = new SqlConnection(Settings.Default.DB_CINEMA);
				conn.Open();
				SqlCommand cmd = new SqlCommand();
				cmd.CommandText = " select 1 from sysobjects where name in ('usuarios', 'filmes', 'salas', 'sessoes', 'configs' ) and xtype='U'  ";
				cmd.Connection = conn;
				SqlDataAdapter da = new SqlDataAdapter();
				da.SelectCommand = cmd;
				DataSet ds = new DataSet();
				da.Fill(ds);
				if (ds.Tables[0].Rows.Count == 5) //if database exist
				{
					msg_tabelas = "Todas as tabelas foram criadas";
					create_database = false;
					update_tables = false;
					drop_database = true;
					drop_table = true;
					status_table_ok = true;
				} else { //if not database exist 
					msg_tabelas = "Não foram criadas/localizadas todas as tabelas ('usuarios', 'filmes', 'salas', 'sessoes', 'configs' )";
					create_database = false;
					update_tables = true;
					drop_database = true;
					drop_table = false;
				}
			} catch (Exception ex) {
				msg_erro = ex.Message;
			} finally {
				if (conn != null) {
					conn.Close();
				}
				if (rdr != null) {
					rdr.Close();
				}
			}

			Object resultado = new {
				msg_database = msg_database,
				msg_tabelas = msg_tabelas,
				msg_erro = msg_erro,
				create_database = create_database,
				update_tables = update_tables,
				drop_database = drop_database,
				drop_table = drop_table,
				status = status_table_ok && status_db_ok
			};
			return resultado;
		}

		public static Object createDatabase() {
			StringBuilder str = new StringBuilder();
			str.Clear();
			str.Append(" IF NOT EXISTS(SELECT * FROM sys.databases WHERE name = 'CINEMA') ");
			str.Append(" CREATE DATABASE CINEMA ");

			msg_erro = runQuery(Settings.Default.DB_MASTER, str);

			return checkDados();
		}

		public static Object UpdateTables() {
			StringBuilder str = new StringBuilder();
			str.Clear();
			/* CRIA TABELA DE USUARIOS */
			str.Append(" if not exists (select * from sysobjects where name='usuarios' and xtype='U') ");
			str.Append(" CREATE TABLE  usuarios ( ");
			str.Append(" 	id int NOT NULL IDENTITY(1,1) PRIMARY KEY, ");
			str.Append("     username varchar(50), ");
			str.Append("     lastname varchar(50), ");
			str.Append("     email varchar(50), ");
			str.Append(" 	 password varchar(50)); ");

			/* POPULA TABELA DE USUARIOS */
			str.Append(" if not exists (SELECT 1 FROM USUARIOS WHERE username = 'admin') insert into usuarios values ('admin', ' do sistema', 'admin@admin.com', '123') ");
			str.Append(" if not exists (SELECT 1 FROM USUARIOS WHERE username = 'gerente')  insert into usuarios values  ('gerente', ' do sistema', 'gerente@gerente.com', '123') ");

			/* CRIA TABELA DE FILMES*/
			str.Append(" if not exists (select * from sysobjects where name='filmes' and xtype='U') ");
			str.Append(" CREATE TABLE filmes ( ");
			str.Append(" 	id int NOT NULL IDENTITY(1,1) PRIMARY KEY, ");
			str.Append("     titulo varchar(150), ");
			str.Append(" 	descricao varchar(1000), ");
			str.Append(" 	imagem varchar(max), ");
			str.Append(" 	duracao_min int,  ");
			str.Append(" 	em_cartaz bit,  ");
			str.Append(" 	deleted bit DEFAULT 0); ");
			str.Append(" delete from filmes; ");

			/* CRIA TABELA DE SALAS */
			str.Append("if not exists (select * from sysobjects where name='salas' and xtype='U') ");
			str.Append("CREATE TABLE salas ( ");
			str.Append("	id int NOT NULL IDENTITY(1,1) PRIMARY KEY, ");
			str.Append("    nome varchar(150), ");
			str.Append("	qtd_assentos int )");

			str.Append(" delete from salas; ");
			str.Append(" insert into salas values ('Sala 01', 45); ");
			str.Append(" insert into salas values ('Sala 01B', 45); ");
			str.Append(" insert into salas values ('Sala 02', 90); ");
			str.Append(" insert into salas values ('Sala 03', 70); ");
			str.Append(" insert into salas values ('Sala 04', 70); ");

			/* CRIA TABELA DE SESSOES */
			str.Append(" if not exists (select * from sysobjects where name='sessoes' and xtype='U') ");
			str.Append(" CREATE TABLE sessoes ( ");
			str.Append(" 	id int NOT NULL IDENTITY(1,1) PRIMARY KEY, ");
			str.Append("    data_hora datetime, ");
			str.Append(" 	valor_ingresso float, ");
			str.Append(" 	tp_animacao varchar(2), ");
			str.Append(" 	tp_audio varchar(1), ");
			str.Append(" 	id_filme int, ");
			str.Append(" 	id_sala int, ");
			str.Append(" 	deleted bit DEFAULT 0); ");
			str.Append(" delete from sessoes; ");

			/* CRIA TABELA DE CONFIGS */
			str.Append(" if not exists (select * from sysobjects where name='configs' and xtype='U') ");
			str.Append(" CREATE TABLE  configs (limpeza_salas_minutos int, tempo_deleta_sessoes int); ");
			str.Append(" delete from configs; ");
			str.Append(" insert into configs values(10, 14400); ");

			msg_erro = runQuery(Settings.Default.DB_CINEMA, str);

			return checkDados();
		}

		public static Object DropTables() {
			StringBuilder str = new StringBuilder();
			str.Clear();
			str.Append(" if exists (select * from sysobjects where name='usuarios' and xtype='U') ");
			str.Append(" drop table usuarios ");

			str.Append(" if exists (select * from sysobjects where name='filmes' and xtype='U') ");
			str.Append(" drop table filmes ");

			str.Append("if exists (select * from sysobjects where name='salas' and xtype='U') ");
			str.Append("drop table salas ");

			str.Append(" if exists (select * from sysobjects where name='sessoes' and xtype='U') ");
			str.Append(" drop table sessoes ");

			str.Append(" if exists (select * from sysobjects where name='configs' and xtype='U') ");
			str.Append(" drop table configs ");

			msg_erro = runQuery(Settings.Default.DB_CINEMA, str);

			return checkDados();
		}

		public static Object DropDatabase() {
			StringBuilder str = new StringBuilder();
			str.Clear();
			str.Append(" IF EXISTS(SELECT * FROM sys.databases WHERE name = 'CINEMA') ");
			str.Append(" DROP DATABASE CINEMA ");

			msg_erro = runQuery(Settings.Default.DB_MASTER, str);

			return checkDados();
		}

		protected static string runQuery(string database, StringBuilder str) {
			SqlConnection conn = null;
			SqlDataReader rdr = null;
			try {
				conn = new SqlConnection(database);
				conn.Open();
				SqlCommand cmd = new SqlCommand(str.ToString(), conn);
				cmd.CommandType = System.Data.CommandType.Text;
				rdr = cmd.ExecuteReader();
			} catch (Exception ex) {
				return ex.Message;
			} finally {
				if (conn != null) {
					conn.Close();
				}
				if (rdr != null) {
					rdr.Close();
				}

			}
			return "";
		}

	}
}