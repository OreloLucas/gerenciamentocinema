﻿using GerenciamentoCinema.Controllers;
using GerenciamentoCinema.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace GerenciamentoCinema.Models {
	public class SessaoModel {
		/* variaveis do obj bd*/
		public int id { get; set; }
		public DateTime data_hora { get; set; }

		[Required(ErrorMessage = "Preencha valor entre 0 e 999")]
		[Range(0, 999, ErrorMessage = "Preencha valor entre 0 e 999")]
		[Display(Name = "Valor Ingresso(R$)")]
		public double valor_ingresso { get; set; }

		public string tp_animacao { get; set; }
		public string tp_audio { get; set; }
		[Required(ErrorMessage = "Preencha o filme")]
		[Display(Name = "Filme")]
		public int id_filme { get; set; }
		public int id_sala { get; set; }

		/* variaveis do obj calculado*/
		[Display(Name = "Data/Hora da sessão")]
		public string data_hora_formatada { get; set; }

		[Display(Name = "Data/Hora Final da sessão")]
		public string data_hora_formatada_final { get; set; }

		public string timepicker { get; set; }
		public string datepicker { get; set; }


		/*dados uteis referente a sala*/
		public string nome_sala { get; set; }
		public int qtd_assentos { get; set; }

		/*dados uteis referente ao filme*/
		[Display(Name = "Duração do filme(Minutos)")]
		public int duracao { get; set; }
		public string imagem { get; set; }
		public string duracao_extenso { get; set; } 
		public string em_cartaz_extenso { get; set; }
		public string titulo_filme { get; set; }
		/*identifica se pode ser deletado ou não*/
		public bool canDelete { get; set; }
		public string canDeleteMSG { get; set; }
		/* dados config*/
		[Display(Name = "Tempo de limpeza da sala")]
		public int tempo_limpeza { get; set; }

		public static List<SessaoModel> list() {
			List<SessaoModel> list = new List<SessaoModel>();

			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			conn.Open();
			SqlCommand cmd = new SqlCommand();
			StringBuilder s = new StringBuilder();
			s.Clear();
			s.Append(" SELECT ");
			s.Append(" sessoes.id, ");
			s.Append(" data_hora,");
			s.Append(" DATEADD(minute,filmes.duracao_min,data_hora) as data_hora_final,");
			s.Append(" FORMAT (data_hora, 'dd/MM/yyyy HH:mm:ss ') as data_hora_formatada,");
			s.Append(" FORMAT (DATEADD(minute,filmes.duracao_min,data_hora), 'dd/MM/yyyy HH:mm:ss ') as data_hora_formatada_final,");
			s.Append(" valor_ingresso,");
			s.Append(" tp_animacao, case when tp_audio = 'D' then 'Dublado' else 'Legendado' end as tp_audio,");
			s.Append(" sessoes.id_filme, sessoes.id_sala,");
			s.Append(" filmes.titulo as titulo_filme, salas.nome as nome_sala");
			s.Append(" FROM sessoes ");
			s.Append(" where deleted = 0 ");
			s.Append(" inner join filmes on (filmes.id = sessoes.id_filme)");
			s.Append(" inner join salas on (salas.id = sessoes.id_sala) ");


			cmd.CommandText = s.ToString();
			cmd.Connection = conn;

			using (SqlDataReader reader = cmd.ExecuteReader()) {
				while (reader.Read()) {
					SessaoModel a = new SessaoModel();
					a.id = (int)reader["id"];
					a.data_hora_formatada = (string)reader["data_hora_formatada"];
					a.data_hora_formatada_final = (string)reader["data_hora_formatada_final"];
					a.valor_ingresso = (double)reader["valor_ingresso"];
					a.tp_animacao = (string)reader["tp_animacao"];
					a.tp_audio = (string)reader["tp_audio"];

					a.titulo_filme = (string)reader["titulo_filme"];
					a.nome_sala = (string)reader["nome_sala"];


					a.id_filme = (int)reader["id_filme"];
					a.id_sala = (int)reader["id_sala"];
					list.Add(a);
				}
			}
			conn.Close();
			return list;
		}

		public static List<SessaoModel> listLimmit(int i, string filtro_date) {
			List<SessaoModel> list = new List<SessaoModel>();
			switch (filtro_date) {
				case "passado":
					filtro_date = "AND cast(sessoes.data_hora as date) < cast(GETDATE() as date) ";
					break; //somente com data menor a hoje
				case "hoje":
					filtro_date = "AND cast(sessoes.data_hora as date) = cast(GETDATE() as date) ";
					break; //somente com data igual a hoje
				case "futuras":
					filtro_date = " AND cast(sessoes.data_hora as date) > cast(GETDATE() as date) ";
					break; //somente com data anterior a hoje
				default:
					filtro_date = " AND 1=1 ";
					break; //Sem restrição de data
			};

			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			conn.Open();
			SqlCommand cmd = new SqlCommand();
			StringBuilder str = new StringBuilder();
			str.Clear();
			str.Append(" declare ");
			str.Append(" @i int, ");
			str.Append(" @limpeza_sala int ");
			str.Append(" set @i = " + i.ToString());
			str.Append(" set @limpeza_sala = (select limpeza_salas_minutos from configs) ");
			str.Append(" SELECT ");
			str.Append(" sessoes.id, ");
			str.Append(" data_hora,");
			str.Append(" DATEADD(minute,(filmes.duracao_min + @limpeza_sala),data_hora) as data_hora_final,");
			str.Append(" FORMAT (data_hora, 'dd/MM/yyyy HH:mm:ss ') as data_hora_formatada,");
			str.Append(" FORMAT (DATEADD(minute,(filmes.duracao_min + @limpeza_sala),data_hora), 'dd/MM/yyyy HH:mm:ss ') as data_hora_formatada_final,");
			str.Append(" valor_ingresso,");
			str.Append(" tp_animacao, case when tp_audio = 'D' then 'Dublado' else 'Legendado' end as tp_audio,");
			str.Append(" sessoes.id_filme, sessoes.id_sala,");
			str.Append(" filmes.titulo as titulo_filme, salas.nome as nome_sala");
			str.Append(" FROM sessoes ");
			str.Append(" inner join filmes on (filmes.id = sessoes.id_filme)");
			str.Append(" inner join salas on (salas.id = sessoes.id_sala) ");
			str.Append(" where sessoes.deleted = 0 ");
			str.Append(filtro_date);
			str.Append(" order by sessoes.id ");
			str.Append(" OFFSET (@i*" + GeneralController.MaxPPag + ")-" + +GeneralController.MaxPPag + " ROWS ");
			str.Append(" FETCH NEXT " + GeneralController.MaxPPag + " ROWS ONLY; ");

			cmd.CommandText = str.ToString();

			cmd.Connection = conn;

			using (SqlDataReader reader = cmd.ExecuteReader()) {
				while (reader.Read()) {
					SessaoModel a = new SessaoModel();
					a.id = (int)reader["id"];
					a.data_hora_formatada = (string)reader["data_hora_formatada"];
					a.data_hora_formatada_final = (string)reader["data_hora_formatada_final"];
					a.valor_ingresso = (double)reader["valor_ingresso"];
					a.tp_animacao = (string)reader["tp_animacao"];
					a.tp_audio = (string)reader["tp_audio"];

					a.titulo_filme = (string)reader["titulo_filme"];
					a.nome_sala = (string)reader["nome_sala"];


					a.id_filme = (int)reader["id_filme"];
					a.id_sala = (int)reader["id_sala"];
					list.Add(a);
				}
			}
			conn.Close();
			return list;
		}

		public static List<Object> checkHorarioValido(string inicio, int idsala, int id_filme, int id_sessao = 0) {
			List<Object> list = new List<object>();

			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			conn.Open();
			SqlCommand cmd = new SqlCommand();
			StringBuilder str = new StringBuilder();
			str.Clear();
			str.Append(" declare ");
			str.Append(" @id_sessao int,");
			str.Append(" @id_sala int,");
			str.Append(" @dt datetime,");
			str.Append(" @dt_fim datetime, ");
			str.Append(" @limpeza_sala int ");
			str.Append(" set @id_sessao = " + id_sessao.ToString());
			str.Append(" set @id_sala = " + idsala.ToString());
			str.Append(" set @dt = '" + inicio + "'");
			str.Append(" set @limpeza_sala = (select limpeza_salas_minutos from configs) ");
			str.Append(" set @dt_fim = (select DATEADD(minute,(filmes.duracao_min + @limpeza_sala), @dt) from filmes where filmes.id = " + id_filme.ToString() + ")");
			str.Append(" select FORMAT (data_hora, 'dd/MM/yyyy HH:mm:ss ') as data_hora_formatada,");
			str.Append("  FORMAT (DATEADD(minute,(filmes.duracao_min + @limpeza_sala),data_hora), 'dd/MM/yyyy HH:mm:ss ') as data_hora_formatada_final,");
			str.Append("  salas.nome, filmes.titulo");
			str.Append(" from sessoes ");
			str.Append(" inner join filmes on (filmes.id = sessoes.id_filme)");
			str.Append(" inner join salas on (salas.id = sessoes.id_sala)");
			str.Append(" where sessoes.id_sala = @id_sala and sessoes.deleted = 0");
			if (id_sessao > 0) {
				str.Append(" and sessoes.id <> @id_sessao");
			}
			str.Append(" and ((@dt BETWEEN data_hora and DATEADD(minute,(filmes.duracao_min + @limpeza_sala),data_hora)) or (@dt_fim BETWEEN data_hora and DATEADD(minute,(filmes.duracao_min + @limpeza_sala),data_hora)))");
			cmd.CommandText = str.ToString();

			cmd.Connection = conn;

			using (SqlDataReader reader = cmd.ExecuteReader()) {
				while (reader.Read()) {
					list.Add(new {
						msg = "<strong>Horario/sala já ocupado durante o periodo selecionado</strong><br>" +
						"Sala : " + (string)reader["nome"] + "<br>" +
						"Horário inicio : " + (string)reader["data_hora_formatada"] + "<br>" +
						"Horário final : " + (string)reader["data_hora_formatada_final"] + "<br>" +
						 "Filme : " + (string)reader["titulo"] + "<br>"
					});
				}
			}
			conn.Close();
			return list;
		}

		public static string Insert(SessaoModel sessao) {
			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			if (checkHorarioValido(sessao.data_hora_formatada, sessao.id_sala, sessao.id_filme ).Count > 0) {
				return "Já Existe sessão vinculado ao horário/sala";
			}
			try {
				StringBuilder str = new StringBuilder();
				str.Clear();
				str.Append(" DECLARE ");
				str.Append(" @dt datetime, ");
				str.Append(" @valor_ingresso  float,");
				str.Append(" @tp_animacao varchar(2),");
				str.Append(" @tp_audio  varchar(1), ");
				str.Append(" @id_filme int, ");
				str.Append(" @id_sala int, ");
				str.Append(" @check int = 0 ");
				str.Append(" ");
				str.Append(" set @dt = '{0}'");
				str.Append(" set @valor_ingresso = {1}");
				str.Append(" set @tp_animacao = '{2}'");
				str.Append(" set @tp_audio = '{3}'");
				str.Append(" set @id_filme = {4}");
				str.Append(" set @id_sala = {5}");
				str.Append(" INSERT INTO [dbo].[sessoes] ");
				str.Append(" ([data_hora] ,[valor_ingresso] ,[tp_animacao] ,[tp_audio] ,[id_filme] ,[id_sala] ,[deleted]) VALUES ");
				str.Append(" (@dt, @valor_ingresso, @tp_animacao, @tp_audio, @id_filme, @id_sala, 0) ");
				str.Append(" 	if  @@ROWCOUNT>0   ");
				str.Append(" 	set @check = 1");
				str.Append(" 	SELECT @check as checked, case when @check = 0 then 'Erro ao incluir o sessão' else '' end as result ");

				conn.Open();

				SqlCommand cmd = new SqlCommand();
				cmd.CommandText = string.Format(str.ToString(),
					sessao.data_hora_formatada, //0
					sessao.valor_ingresso, //1
					sessao.tp_animacao, //2
					sessao.tp_audio, //3
					sessao.id_filme, //4
					sessao.id_sala //5
					);
				cmd.Connection = conn;
				string result = "";
				using (SqlDataReader reader = cmd.ExecuteReader()) {
					while (reader.Read()) {
						if ((int)(reader["checked"]) == 0) {
							result = (string)reader["result"];
						}
					}
				}
				return result;
			} catch (Exception ex) {
				return ex.Message;
			} finally {
				if (conn != null) {
					conn.Close();
				}
			}

		}

		public static string Update(SessaoModel sessao) {
			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			try {
				StringBuilder str = new StringBuilder();
				str.Clear();
				str.Append(" DECLARE ");
				str.Append(" @id int,");//0
				str.Append(" @data_hora datetime, ");//1
				str.Append(" @valor_ingresso float, ");//2
				str.Append(" @tp_animacao varchar(2),");//3
				str.Append(" @tp_audio varchar(1),"); //4
				str.Append(" @id_filme int,");//5
				str.Append(" @id_sala int,");//6
				str.Append(" @check int = 0");

				str.Append(" set @id = '{0}'");
				str.Append(" set @data_hora = '{1}'");
				str.Append(" set @valor_ingresso = {2}");
				str.Append(" set @tp_animacao = '{3}'");
				str.Append(" set @tp_audio =  '{4}' ");
				str.Append(" set @id_filme =  {5} ");
				str.Append(" set @id_sala =  {6} ");

				//str.Append(" if not exists(select * from filmes where titulo = @titulo and id <> @id)");
				//str.Append(" begin ");
				str.Append(" UPDATE SESSOES SET data_hora = @data_hora, valor_ingresso = @valor_ingresso, tp_animacao = @tp_animacao, tp_audio = @tp_audio, id_filme = @id_filme, id_sala = @id_sala  where id = @id;");
				str.Append(" if  @@ROWCOUNT>0   ");
				str.Append(" set @check = 1");
				str.Append(" SELECT @check as checked, case when @check = 0 then 'Erro ao incluir o sessao' else '' end as result ");
				//str.Append(" end");
				//str.Append(" else");
				//str.Append(" begin");
				//str.Append(" SELECT @check as checked,  'Filme ''' + @titulo + ''' já cadastrado' as result ");
				//str.Append(" end");

				conn.Open();

				SqlCommand cmd = new SqlCommand();
				cmd.CommandText = string.Format(str.ToString(),
					sessao.id, //0
					sessao.data_hora_formatada,  //1
					sessao.valor_ingresso, //2
					sessao.tp_animacao, //3
					sessao.tp_audio,//4
					sessao.id_filme,//5
					sessao.id_sala); //6
				cmd.Connection = conn;
				string result = "";
				using (SqlDataReader reader = cmd.ExecuteReader()) {
					while (reader.Read()) {
						if ((int)(reader["checked"]) == 0) {
							result = (string)reader["result"];
						}
					}
				}
				return result;
			} catch (Exception ex) {
				return ex.Message;
			} finally {
				if (conn != null) {
					conn.Close();
				}
			}
		}

		public static SessaoModel locateByID(int id) {
			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			conn.Open();
			SqlCommand cmd = new SqlCommand();
			StringBuilder str = new StringBuilder();
			str.Clear();
			str.Append(" declare @id int, @valToDelete int, @limpeza_sala int  ");
			str.Append(" set @id = {0} ");
			str.Append(" set @limpeza_sala = (select limpeza_salas_minutos from configs) ");
			str.Append(" set @valToDelete = (select tempo_deleta_sessoes from configs)");
			str.Append(" select sessoes.id, ");
			str.Append(" data_hora,");
			str.Append(" DATEADD(minute,(filmes.duracao_min + @limpeza_sala),data_hora) as data_hora_final,");
			str.Append(" FORMAT(data_hora, 'dd/MM/yyyy HH:mm:ss ') as data_hora_formatada,");
			str.Append(" FORMAT(data_hora, 'dd/MM/yyyy') as datepicker, FORMAT(data_hora, 'HH:mm') as timepicker, ");
			str.Append(" FORMAT (DATEADD(minute,(filmes.duracao_min + @limpeza_sala),data_hora), 'dd/MM/yyyy HH:mm:ss ') as data_hora_formatada_final,");
			str.Append(" valor_ingresso, tp_animacao, case when tp_audio = 'D' then 'Dublado' else 'Legendado' end as tp_audio,");
			str.Append(" salas.id as id_sala, salas.nome as nome_sala, salas.qtd_assentos, "); // dados da sala
			str.Append(" sessoes.id_filme, filmes.duracao_min as duracao, filmes.titulo as titulo_filme, filmes.duracao_min  as duracao, cast( filmes.imagem as XML ) as imagem, "); // dados do filme
			str.Append(" (case when  filmes.duracao_min/ 60 > 0 then CAST(( filmes.duracao_min / 60) as varchar(10))  + 'h ' else '' end + CAST(( filmes.duracao_min % 60) as varchar(10)) + 'min' + '(filme) ' ) +");
			str.Append(" (select case when(limpeza_salas_minutos / 60 > 0) then(CAST((limpeza_salas_minutos / 60) as varchar(100)) + 'h ') else ''end + case when(limpeza_salas_minutos % 60 > 0) then(CAST((limpeza_salas_minutos % 60) as varchar(100)) + 'min') else ''end + '(limpeza)' From configs)");
			str.Append(" as duracao_extenso, ");
			str.Append(" case when filmes.em_cartaz = 1 then 'Em cartaz' else 'Fora de cartaz' end as em_cartaz_extenso, ");
			str.Append(" (select limpeza_salas_minutos from configs) as tempo_limpeza, ");
			/* verifica se pode ser deletado */
			str.Append(" CASE  ");
			str.Append(" WHEN DATEDIFF(MINUTE, CAST(GETDATE() AS Datetime), sessoes.data_hora) < 0 THEN  'Não pode deletado pois sessão já iniciou' ");
			str.Append(" WHEN (DATEDIFF(MINUTE, CAST(GETDATE() AS Datetime), sessoes.data_hora) < @valToDelete)  THEN 'Não pode deletado pois a sessão inicia dentro de ' + ");
			str.Append(" (select  ");
			str.Append(" case when ((tempo_deleta_sessoes/ 1440) > 0) then (CAST(tempo_deleta_sessoes/ 1440 as varchar(100)) + ' dia(s) ') else ''end + ");
			str.Append(" case when ((tempo_deleta_sessoes/ 60) -  ((tempo_deleta_sessoes/ 1440) * 24) > 0) then (CAST((tempo_deleta_sessoes/ 60) -  ((tempo_deleta_sessoes/ 1440) * 24) as varchar(100)) + ' hora(s) ') else ''end + ");
			str.Append(" case when (tempo_deleta_sessoes % 60 > 0) then (CAST((tempo_deleta_sessoes % 60) as varchar(100)) + ' minuto(s) ') else ''end ");
			str.Append(" From configs) + ' (Limite definido pelas configurações do sistema)' ");
			str.Append(" else 'Pode ser deletado' ");
			str.Append(" end as canDeleteMSG, ");
			str.Append(" case when (DATEDIFF(MINUTE, CAST( GETDATE() AS Datetime ), sessoes.data_hora) > @valToDelete) then  1 else 0 end as canDelete ");
			/* verifica se pode ser deletado */
			str.Append(" FROM sessoes ");
			str.Append(" inner join filmes on (filmes.id = sessoes.id_filme)");
			str.Append(" inner join salas on (salas.id = sessoes.id_sala) ");
			str.Append(" where sessoes.id = @id and sessoes.deleted = 0 ");

			cmd.CommandText = string.Format(str.ToString(), id);
			cmd.Connection = conn;
			SessaoModel a = new SessaoModel();
			using (SqlDataReader reader = cmd.ExecuteReader()) {
				while (reader.Read()) {
					a.id = (int)reader["id"];
					a.data_hora_formatada = (string)reader["data_hora_formatada"];
					a.data_hora_formatada_final = (string)reader["data_hora_formatada_final"];
					a.datepicker = (string)reader["datepicker"];
					a.timepicker = (string)reader["timepicker"];
					a.titulo_filme = (string)reader["titulo_filme"];
					a.id_filme = (int)reader["id_filme"];
					a.id_sala = (int)reader["id_sala"];
					a.nome_sala = (string)reader["nome_sala"];
					a.qtd_assentos = (int)reader["qtd_assentos"];
					a.tp_animacao = (string)reader["tp_animacao"];
					a.tp_audio = (string)reader["tp_audio"];
					a.valor_ingresso = (double)reader["valor_ingresso"];
					a.duracao_extenso = (string)reader["duracao_extenso"];
					a.em_cartaz_extenso = (string)reader["em_cartaz_extenso"];
					a.canDeleteMSG = (string)reader["canDeleteMSG"];
					a.canDelete = (bool)((int)reader["canDelete"] == 1 ? true : false);
					a.imagem = (string)reader["imagem"];

					a.tempo_limpeza = (int)reader["tempo_limpeza"];
					a.duracao = (int)reader["duracao"];
				}
			}
			conn.Close();
			return a;
		}


		public static int locadeIDByFilmeSalaDataHora(int id_filme, int id_sala, string datahora) {
			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			conn.Open();
			SqlCommand cmd = new SqlCommand();
			StringBuilder str = new StringBuilder();
			str.Clear();
			str.Append(" declare @id_filme int, @id_sala int, @datahora datetime  ");
			str.Append(" set @id_filme = {0} ");
			str.Append(" set @id_sala = {1} ");
			str.Append(" set @datahora = '{2}' ");
			str.Append(" select top 1 sessoes.id "); 
			str.Append(" FROM sessoes ");
			str.Append(" inner join filmes on (filmes.id = sessoes.id_filme)");
			str.Append(" inner join salas on (salas.id = sessoes.id_sala) ");
			str.Append(" where sessoes.id_filme = @id_filme and sessoes.id_sala = @id_sala and data_hora = @datahora and sessoes.deleted = 0 ");

			cmd.CommandText = string.Format(str.ToString(), id_filme, id_sala, datahora);
			cmd.Connection = conn;
			int result = 0;
			using (SqlDataReader reader = cmd.ExecuteReader()) {
				while (reader.Read()) {
					result = (int)reader["id"];
				}
			}
			conn.Close();
			return result;
		}


		public static string delete(int id) {
			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			string result = "";
			try {
				conn.Open();
				SqlCommand cmd = new SqlCommand();
				StringBuilder str = new StringBuilder();
				str.Clear();
				str.Append(" declare  ");
				str.Append(" @check int,");
				str.Append(" @valToDelete int,");
				str.Append(" @id int");
				str.Append(" set @valToDelete = (select tempo_deleta_sessoes from configs)");
				str.Append(" set @id = {0}");
				str.Append(" set @check = (select case when (DATEDIFF(MINUTE, CAST( GETDATE() AS Datetime ), sessoes.data_hora) > @valToDelete) then  1 else 0 end from sessoes where id = @id)");
				str.Append(" if (@check = 1)");
				str.Append(" begin");
				str.Append(" 	update sessoes set deleted = 1 where id = @id");
				str.Append(" 	select '' as result");
				str.Append(" end");
				str.Append(" else");
				str.Append(" 	select 'Sessão fora dos requisitos para deletar' as result ");


				cmd.CommandText = string.Format(str.ToString(), id);
				cmd.Connection = conn;

				using (SqlDataReader reader = cmd.ExecuteReader()) {
					while (reader.Read()) {
						result = (string)reader["result"];
					}
				}

			} catch (Exception ex) {
				return ex.Message;
			} finally {
				if (conn != null) {
					conn.Close();
				}

			}
			return result;
		}

		public static Boolean HardDelete() {
			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			try {
				conn.Open();
				SqlCommand cmd = new SqlCommand();
				StringBuilder str = new StringBuilder();
				str.Clear();

				str.Append(" delete from sessoes where deleted = 1 ");
				cmd.CommandText = str.ToString();
				cmd.Connection = conn;
				cmd.ExecuteReader();

			} catch (Exception ex) {
				return false;
			} finally {
				if (conn != null) {
					conn.Close();
				}
			}
			return true;
		}

	}
}