﻿using GerenciamentoCinema.Controllers;
using GerenciamentoCinema.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace GerenciamentoCinema.Models {
	public class FilmeModel {
		public int id { get; set; }

		[Required(ErrorMessage = "Informe um titulo")]
		[Display(Name = "Titulo")]
		public string titulo { get; set; }

		[Required(ErrorMessage = "Informe uma descrição")]
		[Display(Name = "Descrição")]
		public string descricao { get; set; }

		[Required(ErrorMessage = "Preencha duranção entre 0 e 999")]
		[Range(1, 999, ErrorMessage = "Preencha duranção entre 0 e 999")]
		[Display(Name = "Duração(minutos)")]
		public int duracao_min { get; set; }

		[Display(Name = "Em Cartaz")]
		public bool em_cartaz { get; set; }

		public string em_cartaz_extenso { get; set; }
		public string duracao_extenso { get; set; }

		public string imagem { get; set; }

		public static List<FilmeModel> listLimmit(int i, string consulta = "", string filtro_personalizado = "") {
			List<FilmeModel> list = new List<FilmeModel>();

			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			conn.Open();
			SqlCommand cmd = new SqlCommand();
			StringBuilder str = new StringBuilder();
			str.Clear();
			str.Append(" declare ");
			str.Append(" @i int, ");
			str.Append(" @consulta varchar(100) ");
			str.Append(" set @i = {0}");
			str.Append(" set @consulta = '%{1}%'");
			str.Append(" select id, titulo, descricao, duracao_min,");
			str.Append(" case when duracao_min/ 60 > 0 then CAST((duracao_min / 60) as varchar(10))  +'h ' else '' end + CAST((duracao_min % 60) as varchar(10)) + 'min' as duracao_extenso, ");
			str.Append(" case when em_cartaz = 1 then 'Em cartaz' else 'Fora de cartaz' end as em_cartaz_extenso ");
			str.Append(" from filmes ");
			str.Append(" where deleted = 0");
			str.Append(" AND (UPPER(titulo) LIKE @consulta or UPPER(descricao) LIKE @consulta or UPPER(duracao_min) LIKE @consulta) ");
			str.Append(filtro_personalizado);
			str.Append(" ORDER BY ID ");
			str.Append(" OFFSET (@i*{2})-{2} ROWS ");
			str.Append(" FETCH NEXT {2} ROWS ONLY; ");

			cmd.CommandText = string.Format(str.ToString(), i,  //0
				consulta.ToUpper(), //1
				GeneralController.MaxPPag); //2


			cmd.Connection = conn;

			using (SqlDataReader reader = cmd.ExecuteReader()) {
				while (reader.Read()) {
					FilmeModel a = new FilmeModel();
					a.id = (int)reader["id"];
					a.titulo = (string)reader["titulo"];
					a.descricao = (string)reader["descricao"];
					a.duracao_extenso = (string)reader["duracao_extenso"];
					a.duracao_min = (int)reader["duracao_min"];
					a.em_cartaz_extenso = (string)reader["em_cartaz_extenso"];
					list.Add(a);
				}
			}
			conn.Close();
			return list;
		}

		public static string Insert(FilmeModel filme) {
			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);

			try {
				StringBuilder str = new StringBuilder();
				str.Clear();
				str.Append(" DECLARE ");
				str.Append(" @titulo varchar(150), ");
				str.Append(" @descricao varchar(1000),");
				str.Append(" @duracao_min int,");
				str.Append(" @em_cartaz int, ");
				str.Append(" @imagem varchar(max), ");
				str.Append(" @check int = 0 ");
				str.Append(" ");
				str.Append(" set @titulo = '{0}'");
				str.Append(" set @descricao = '{1}'");
				str.Append(" set @duracao_min =  {2}");
				str.Append(" set @em_cartaz = {3}");
				str.Append(" set @imagem =");
				str.Append("'" + filme.imagem + "'");
				str.Append(" if not exists(select 1 from filmes where titulo = @titulo and deleted = 0)");
				str.Append(" begin ");
				str.Append(" 	INSERT INTO [dbo].[filmes] ([titulo] , [descricao] , [duracao_min], [em_cartaz], [imagem]) VALUES (@titulo, @descricao, @duracao_min, @em_cartaz, @imagem) ");
				str.Append(" 	if  @@ROWCOUNT>0   ");
				str.Append(" 	set @check = 1");
				str.Append(" 	SELECT @check as checked, case when @check = 0 then 'Erro ao incluir o filme' else '' end as result ");
				str.Append(" end");
				str.Append(" else");
				str.Append(" begin");
				str.Append("	SELECT @check as checked,  'Filme ''' + @titulo + ''' já cadastrado' as result ");
				str.Append(" end");

				conn.Open();

				SqlCommand cmd = new SqlCommand();
				cmd.CommandText = string.Format(str.ToString(), filme.titulo,  //0
					filme.descricao, //1
					filme.duracao_min, //2
					(filme.em_cartaz ? 1 : 0)); //3
				cmd.Connection = conn;
				string result = "";
				using (SqlDataReader reader = cmd.ExecuteReader()) {
					while (reader.Read()) {
						if ((int)(reader["checked"]) == 0) {
							result = (string)reader["result"];
						}
					}
				}
				return result;
			} catch (Exception ex) {
				return ex.Message;
			} finally {
				if (conn != null) {
					conn.Close();
				}
			}

		}

		public static string Update(FilmeModel filme) {
			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			try {
				StringBuilder str = new StringBuilder();
				str.Clear();
				str.Append(" DECLARE ");
				str.Append(" @id int,");
				str.Append(" @titulo varchar(150), ");
				str.Append(" @descricao varchar(1000),");
				str.Append(" @duracao_min int, ");
				str.Append(" @em_cartaz int, ");
				str.Append(" @imagem varchar(max), ");
				str.Append(" @check int = 0");
				str.Append(" set @id = '{0}'");
				str.Append(" set @titulo = '{1}'");
				str.Append(" set @descricao = '{2}'");
				str.Append(" set @duracao_min = {3}");
				str.Append(" set @em_cartaz =  {4} ");
				str.Append(" set @imagem = ");
				str.Append("'" + filme.imagem + "'");
				str.Append(" if not exists(select * from filmes where titulo = @titulo and id <> @id)");
				str.Append(" begin ");
				str.Append(" UPDATE FILMES SET titulo = @titulo, descricao = @descricao, duracao_min = @duracao_min, em_cartaz = @em_cartaz, imagem = @imagem where id = @id;");
				str.Append(" if  @@ROWCOUNT>0   ");
				str.Append(" set @check = 1");
				str.Append(" SELECT @check as checked, case when @check = 0 then 'Erro ao incluir o filme' else '' end as result ");
				str.Append(" end");
				str.Append(" else");
				str.Append(" begin");
				str.Append(" SELECT @check as checked,  'Filme ''' + @titulo + ''' já cadastrado' as result ");
				str.Append(" end");

				conn.Open();

				SqlCommand cmd = new SqlCommand();
				cmd.CommandText = string.Format(str.ToString(),
					filme.id, //0
					filme.titulo,  //1
					filme.descricao, //2
					filme.duracao_min, //3
					(filme.em_cartaz ? 1 : 0)); //4
				cmd.Connection = conn;
				string result = "";
				using (SqlDataReader reader = cmd.ExecuteReader()) {
					while (reader.Read()) {
						if ((int)(reader["checked"]) == 0) {
							result = (string)reader["result"];
						}
					}
				}
				return result;
			} catch (Exception ex) {
				return ex.Message;
			} finally {
				if (conn != null) {
					conn.Close();
				}
			}

		}

		public static FilmeModel locateByID(int id) {
			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			conn.Open();
			SqlCommand cmd = new SqlCommand();
			StringBuilder str = new StringBuilder();
			str.Clear();
			str.Append(" declare  @id int set @id = {0}");
			str.Append(" SELECT top 1 id, titulo, descricao, duracao_min, em_cartaz, ");
			str.Append(" case when duracao_min/ 60 > 0 then CAST((duracao_min / 60) as varchar(10))  +'h ' else '' end + CAST((duracao_min % 60) as varchar(10)) + 'min' as duracao_extenso, ");
			str.Append(" case when em_cartaz = 1 then 'Em cartaz' else 'Fora de cartaz' end as em_cartaz_extenso, ");
			str.Append(" cast( imagem as XML ) as imagem");
			str.Append(" FROM filmes ");
			str.Append(" where id = @id and deleted = 0 ");

			cmd.CommandText = string.Format(str.ToString(), id);
			cmd.Connection = conn;
			FilmeModel a = new FilmeModel();
			using (SqlDataReader reader = cmd.ExecuteReader()) {
				while (reader.Read()) {
					a.id = (int)reader["id"];
					a.titulo = (string)reader["titulo"];
					a.descricao = (string)reader["descricao"];
					a.duracao_min = (int)reader["duracao_min"];
					a.duracao_extenso = (string)reader["duracao_extenso"];
					a.em_cartaz = (bool)reader["em_cartaz"];
					a.em_cartaz_extenso = (string)reader["em_cartaz_extenso"];
					a.imagem = (string)reader["imagem"];
				}
			}
			conn.Close();
			return a;
		}

		public static int locateIDByTitulo(string name) {
			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			conn.Open();
			SqlCommand cmd = new SqlCommand();
			StringBuilder str = new StringBuilder();
			str.Clear();
			str.Append(" declare  @name varchar(100) set @name = '{0}'");
			str.Append(" SELECT top 1 id, titulo, descricao, duracao_min, em_cartaz, ");
			str.Append(" case when duracao_min/ 60 > 0 then CAST((duracao_min / 60) as varchar(10))  +'h ' else '' end + CAST((duracao_min % 60) as varchar(10)) + 'min' as duracao_extenso, ");
			str.Append(" case when em_cartaz = 1 then 'Em cartaz' else 'Fora de cartaz' end as em_cartaz_extenso, ");
			str.Append(" cast( imagem as XML ) as imagem");
			str.Append(" FROM filmes ");
			str.Append(" where titulo = @name and deleted = 0 ");

			cmd.CommandText = string.Format(str.ToString(), name);
			cmd.Connection = conn;
			int id = 0;
			using (SqlDataReader reader = cmd.ExecuteReader()) {
				while (reader.Read()) {
					id = (int)reader["id"];
				}
			}

			conn.Close();
			return id;
		}

		public static List<Object> canDelete(int id) {
			List<Object> result = new List<object>();


			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			conn.Open();
			SqlCommand cmd = new SqlCommand();
			StringBuilder str = new StringBuilder();
			str.Clear();
			str.Append(" declare @idfilme int ");
			str.Append(" set @idfilme = {0} ");
			str.Append(" select sessoes.id,  ");
			str.Append(" salas.nome, ");
			str.Append(" FORMAT (data_hora, 'dd/MM/yyyy HH:mm:ss ') as data_hora_formatada, ");
			str.Append(" FORMAT (DATEADD(minute,filmes.duracao_min,data_hora), 'dd/MM/yyyy HH:mm:ss ') as data_hora_formatada_final  ");
			str.Append(" from sessoes  ");
			str.Append(" inner join filmes on (filmes.id = sessoes.id_filme) ");
			str.Append(" inner join salas on (salas.id = sessoes.id_sala) ");
			str.Append(" where id_filme = @idfilme ");

			cmd.CommandText = string.Format(str.ToString(), id);
			cmd.Connection = conn;
			result.Add(locateByID(id));
			using (SqlDataReader reader = cmd.ExecuteReader()) {
				while (reader.Read()) {
					int id_result = (int)reader["id"];
					result.Add(new {
						msg = "Sessão vinculada : " + id_result.ToString() + "<br>" +
						"Sala : " + (string)reader["nome"] + "<br>" +
						"Hora inicio sessão : " + (string)reader["data_hora_formatada"] + "<br>" +
						"Hora final sessão : " + (string)reader["data_hora_formatada_final"]
					});
				}
			}
			conn.Close();
			return result;
		}

		public static string delete(int id) {
			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			string result = "";
			try {
				conn.Open();
				SqlCommand cmd = new SqlCommand();
				StringBuilder str = new StringBuilder();
				str.Clear();

				str.Append(" declare  ");
				str.Append(" @check int,");
				str.Append(" @id int ");
				str.Append(" set @id = {0}");
				str.Append(" set @check = (select count(1) from sessoes where id_filme = @id and sessoes.deleted = 0 )");
				str.Append(" if (@check = 0)");
				str.Append(" begin");
				str.Append(" update filmes set deleted = 1 where id = @id ");
				str.Append(" select '' as result");
				str.Append(" end");
				str.Append(" else");
				str.Append(" select 'Filme possui sessão vinculada' as result ");


				cmd.CommandText = string.Format(str.ToString(), id);
				cmd.Connection = conn;

				using (SqlDataReader reader = cmd.ExecuteReader()) {
					while (reader.Read()) {
						result = (string)reader["result"];
					}
				}

			} catch (Exception ex) {
				return ex.Message;
			} finally {
				if (conn != null) {
					conn.Close();
				}

			}
			return result;
		}

		public static Boolean HardDelete() {
			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			try {
				conn.Open();
				SqlCommand cmd = new SqlCommand();
				StringBuilder str = new StringBuilder();
				str.Clear();

				str.Append(" delete from filmes where deleted = 1 ");
				cmd.CommandText = str.ToString();
				cmd.Connection = conn;
				cmd.ExecuteReader();

			} catch (Exception ex) {
				return false;
			} finally {
				if (conn != null) {
					conn.Close();
				}
			}
			return true;
		}

	}
}