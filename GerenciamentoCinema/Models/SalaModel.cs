﻿using GerenciamentoCinema.Controllers;
using GerenciamentoCinema.Properties;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace GerenciamentoCinema.Models {
	public class SalaModel {

		public int id { get; set; }
		public string nome { get; set; }
		public int qtd_assentos { get; set; }

		public static List<SalaModel> list() {
			List<SalaModel> list = new List<SalaModel>();

			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			conn.Open();
			SqlCommand cmd = new SqlCommand();
			StringBuilder str = new StringBuilder();
			str.Clear();
			str.Append(" select id, nome, qtd_assentos from salas ORDER BY ID ");

			cmd.CommandText = str.ToString();
			cmd.Connection = conn;

			using (SqlDataReader reader = cmd.ExecuteReader()) {
				while (reader.Read()) {
					SalaModel a = new SalaModel();
					a.id = (int)reader["id"];
					a.nome = (string)reader["nome"];
					a.qtd_assentos = (int)reader["qtd_assentos"];
					list.Add(a);
				}
			}
			conn.Close();
			return list;
		}

		public static List<SalaModel> listLimmit(int i) {
			List<SalaModel> list = new List<SalaModel>();

			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			conn.Open();
			SqlCommand cmd = new SqlCommand();
			StringBuilder str = new StringBuilder();
			str.Clear();
			str.Append(" declare ");
			str.Append(" @i int");
			str.Append(" set @i = " + i.ToString());
			str.Append(" select id, nome, qtd_assentos from salas ");
			str.Append(" ORDER BY ID ");
			str.Append(" OFFSET (@i*" + GeneralController.MaxPPag + ")-" + GeneralController.MaxPPag + " ROWS ");
			str.Append(" FETCH NEXT " + GeneralController.MaxPPag + " ROWS ONLY; ");

			cmd.CommandText = str.ToString();

			cmd.Connection = conn;

			using (SqlDataReader reader = cmd.ExecuteReader()) {
				while (reader.Read()) {
					SalaModel a = new SalaModel();
					a.id = (int)reader["id"];
					a.nome = (string)reader["nome"];
					a.qtd_assentos = (int)reader["qtd_assentos"];
					list.Add(a);
				}
			}
			conn.Close();
			return list;
		}
	}
}