﻿using GerenciamentoCinema.Properties;
using System;
using System.Data.SqlClient;
using System.Text;
using System.Web.Mvc;

namespace GerenciamentoCinema.Controllers {
	public class ConfigModel
    {
        public int limpeza_salas_minutos { get; set; }
        public int tempo_deleta_sessoes_minutos { get; set; }

        public string limpeza_salas_formatado { get; set; }
        public string tempo_deleta_sessoes_formatado { get; set; }

        public static Tuple<string, string> Save(ConfigModel config)
        {
			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			conn.Open();
			SqlCommand cmd = new SqlCommand();
			StringBuilder str = new StringBuilder();
			str.Clear();
			str.Append(" declare @limpeza int, @deleta int ");
			str.Append(" set @limpeza = " + config.limpeza_salas_minutos + " set @deleta = " + config.tempo_deleta_sessoes_minutos);
			str.Append(" if ((select count(1) from configs) >= 1)  ");
			str.Append(" 	delete from configs  ");
			str.Append(" insert into configs values(@limpeza, @deleta)  ");
			str.Append(" if  @@ROWCOUNT>0   ");
			str.Append(" 	select 'success' as status, 'Configurações atualizadas com sucesso' as msg  ");
			str.Append(" else  ");
			str.Append(" 	select 'error' as status, 'Erro ao atualizar configurações' as msg  ");


			cmd.CommandText = str.ToString();

			cmd.Connection = conn;
			string msg = "", status = "";

			using (SqlDataReader reader = cmd.ExecuteReader())
			{
				while (reader.Read())
				{

					msg = (string)reader["msg"];
					status = (string)reader["status"];
				}
			}
			conn.Close();
			return Tuple.Create(msg, status);
		}

        public static ConfigModel Load()
        {
			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			conn.Open();
			SqlCommand cmd = new SqlCommand();
			StringBuilder str = new StringBuilder();
			str.Clear();
			str.Append(" select  ");
			str.Append(" limpeza_salas_minutos, tempo_deleta_sessoes,  ");
			str.Append(" case when (limpeza_salas_minutos/ 60 > 0) then (CAST((limpeza_salas_minutos/ 60) as varchar(100)) + ' horas ') else ''end + ");
			str.Append(" case when (limpeza_salas_minutos% 60 > 0) then (CAST((limpeza_salas_minutos% 60) as varchar(100)) + ' minutos ') else ''end as limpeza_salas_formatado, ");
			str.Append(" case when ((tempo_deleta_sessoes/ 1440) > 0) then (CAST(tempo_deleta_sessoes/ 1440 as varchar(100)) + ' dia(s) ') else ''end + ");
			str.Append(" case when ((tempo_deleta_sessoes/ 60) -  ((tempo_deleta_sessoes/ 1440) * 24) > 0) then (CAST((tempo_deleta_sessoes/ 60) -  ((tempo_deleta_sessoes/ 1440) * 24) as varchar(100)) + ' hora(s) ') else ''end + ");
			str.Append(" case when (tempo_deleta_sessoes % 60 > 0) then (CAST((tempo_deleta_sessoes % 60) as varchar(100)) + ' minuto(s) ') else ''end  as tempo_deleta_sessoes_formatado");
			str.Append(" From configs ");

			cmd.CommandText = str.ToString();

			cmd.Connection = conn;
			ConfigModel config = new ConfigModel();

			using (SqlDataReader reader = cmd.ExecuteReader())
			{
				while (reader.Read())
				{
					
					config.limpeza_salas_minutos = (int)reader["limpeza_salas_minutos"];
					config.tempo_deleta_sessoes_minutos = (int)reader["tempo_deleta_sessoes"];

					config.limpeza_salas_formatado = (string)reader["limpeza_salas_formatado"];
					config.tempo_deleta_sessoes_formatado = (string)reader["tempo_deleta_sessoes_formatado"];
					
				}
			}
			conn.Close();
			return config;
		}

		public static int getTempoLimpeza() {
			return Load().limpeza_salas_minutos;		 
		}
		

	}
}