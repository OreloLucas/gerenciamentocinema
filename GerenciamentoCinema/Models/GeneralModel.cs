﻿using GerenciamentoCinema.Controllers;
using GerenciamentoCinema.Properties;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace GerenciamentoCinema.Models {
	public class GeneralModel {

		public static Tuple<int, int> getTotalPaginas(string table, string filtro_personal = "") {
			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			conn.Open();
			SqlCommand cmd = new SqlCommand();
			StringBuilder str = new StringBuilder();
			str.Clear();
			str.Append(" declare ");
			str.Append(" @totReg int, ");
			str.Append(" @totPgs int, ");
			str.Append(" @MaxpPag int ");
			str.Append(" set @MaxpPag = " + GeneralController.MaxPPag);
			str.Append(" set @totReg = (select  count(1)  from " + table + filtro_personal + ") ");
			str.Append(" set @totPgs = (@totReg / @MaxpPag)  ");
			str.Append(" if  ((@totReg % @MaxpPag) > 0) ");
			str.Append(" set @totPgs = @totPgs + 1; ");
			str.Append(" IF ( @totPgs <= 0) ");
			str.Append(" set @totPgs = 1; ");
			str.Append(" select @totPgs as totPgs, @totReg as totReg ");

			int totPgs, totReg;
			totPgs = totReg = 0;
			cmd.Connection = conn;
			cmd.CommandText = str.ToString();
			using (SqlDataReader reader = cmd.ExecuteReader()) {
				while (reader.Read()) {
					totPgs = (int)reader["totPgs"];
					totReg = (int)reader["totReg"];
				}
			}
			conn.Close();

			return Tuple.Create(totPgs, totReg);
		}

	}
}