﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using GerenciamentoCinema.Properties;
using GerenciamentoCinema.Controllers;
using System.Text;

namespace GerenciamentoCinema.Models {
	public class UserModel {

		//private static List<UserModel> list

		[Required(ErrorMessage = "Informe o usuário")]
		[Display(Name = "Usuário:")]
		public string user { get; set; }

		[Required(ErrorMessage = "Informe a senha")]
		[DataType(DataType.Password)]
		[Display(Name = "Senha:")]
		public string password { get; set; }


		public string lastname { get; set; }
		public string email { get; set; }
		public int id { get; set; }

		public static List<UserModel> list() {
			List<UserModel> list = new List<UserModel>();

			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			conn.Open();
			SqlCommand cmd = new SqlCommand();

			cmd.CommandText = string.Format(" SELECT id, username, lastname, email  FROM usuarios");
			cmd.Connection = conn;

			using (SqlDataReader reader = cmd.ExecuteReader()) {
				while (reader.Read()) {
					UserModel a = new UserModel();
					a.id = (int)reader["id"];
					a.user = (string)reader["username"];
					a.lastname = (string)reader["lastname"];
					a.email = (string)reader["email"];
					list.Add(a);
				}
			}
			conn.Close();
			return list;
		}

		public static List<UserModel> listLimmit(int i) {
			List<UserModel> list = new List<UserModel>();

			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			conn.Open();
			SqlCommand cmd = new SqlCommand();
			StringBuilder str = new StringBuilder();
			str.Clear();
			str.Append(" declare ");
			str.Append(" @i int");
			str.Append(" set @i = " + i.ToString());
			str.Append(" select * from usuarios ");
			str.Append(" ORDER BY ID ");
			str.Append(" OFFSET (@i*" + GeneralController.MaxPPag + ")-" + GeneralController.MaxPPag + " ROWS ");
			str.Append(" FETCH NEXT " + GeneralController.MaxPPag + " ROWS ONLY; ");

			cmd.CommandText = str.ToString();

			cmd.Connection = conn;

			using (SqlDataReader reader = cmd.ExecuteReader()) {
				while (reader.Read()) {
					UserModel a = new UserModel();
					a.id = (int)reader["id"];
					a.user = (string)reader["username"];
					a.lastname = (string)reader["lastname"];
					a.email = (string)reader["email"];
					list.Add(a);
				}
			}
			conn.Close();
			return list;
		}

		public static string isValidUser(string user, string password) {
			SqlConnection conn = new SqlConnection(Settings.Default.DB_CINEMA);
			conn.Open();
			SqlCommand cmd = new SqlCommand();

			cmd.CommandText = string.Format(" SELECT  TOP 1 username + lastname  FROM usuarios where password like '{0}' and (username like '{1}' or email like '{1}' or (username + lastname) like '{1}' ) ", password, user);
			cmd.Connection = conn;

			var result = "";// cmd.ExecuteReader();
			using (SqlDataReader reader = cmd.ExecuteReader()) {
				while (reader.Read()) {
					 result = reader.GetString(0);
				}
			}
			conn.Close();
			return result;
		}
	}
}